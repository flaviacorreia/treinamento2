<?php

use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'nome' => 'Primeira inserção',
            'nascimento' => '2022/03/24',
            'cpf' => '111.111.111-11',
            'telefone' => '(73) 98888-8888',
            'email' => 'primeeira_insercao@gmail.com',
            'created_at' => now() //USADO PARA GUARDAR HORA DE CRIAÇÃO

        ]);
    }
}
