<?php

namespace App\Http\Controllers;

use App\Estoque;
use App\ProdutoEstoque;
use Illuminate\Http\Request;

use DataTables;
use Redirect;

class EstoqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $estoque = Estoque::get();
        return view('Estoque.index', compact('estoque'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $produto_estoque = ProdutoEstoque::get();
        return view('Estoque.create', compact('produto_estoque'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {

            // salva os dados do cadastro
            $estoque = new Estoque();
            $estoque->fk_produto_estoque = $request->produto;
            $estoque->quantidade = $request->quantidade_produto;
            $estoque->flag = $request->tipo;

            $estoque->save();

            return Redirect::to('/estoque');
        } catch (\Exception  $errors) {
            return "Erro no cadastro.";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $estoque = Estoque::get();

        return Datatables::of($estoque)
            ->editColumn('titulo', function ($estoque) {
                return $estoque->produto_estoque->titulo;
            })
            ->editColumn('valor', function ($estoque) {
                return $estoque->produto_estoque->valor;
            })
            ->editColumn('volume', function ($estoque) {
                return $estoque->produto_estoque->volume;
            })
            ->editColumn('acao', function ($estoque) {
                return '
                <div class="btn-group btn-group-sm">
                    <a href="/estoque/' . $estoque->id . '/edit"
                        class="btn btn-info"
                        title="Editar" data-toggle="tooltip">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <a href="#"
                        class="btn btn-danger btnExcluir"
                        data-id="' . $estoque->id . '"
                        title="Excluir" data-toggle="tooltip">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>';
            })
            ->escapeColumns([0])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estoque = Estoque::find($id);
        $produto_estoque = ProdutoEstoque::get();
        return view('Estoque.edit', compact('estoque', 'produto_estoque'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            // salva os dados de edição
            $estoque = Estoque::find($id);
            $estoque->fk_produto_estoque = $request->produto;
            $estoque->quantidade = $request->quantidade_produto;
            $estoque->flag = $request->tipo;

            $estoque->save();

            return Redirect::to('/estoque');
            //ou
            // return Redirect::to('/estoque/'.$estoque->id.'/edit');
        } catch (\Exception  $errors) {
            return "Erro na edição." . $errors;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            // busca os dados
            $estoque = Estoque::find($id);
            $estoque->delete();
            return response()->json(array('status' => "OK"));
        } catch (\Exception  $erro) {
            return response()->json(array('erro' => "ERRO"));
        }
    }
}
