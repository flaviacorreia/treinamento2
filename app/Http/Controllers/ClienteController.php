<?php

namespace App\Http\Controllers;

use App\Cliente;
use Redirect;
use Session;
use DataTables;

use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cliente = Cliente::get();
        return view('Cliente.index', compact('cliente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cliente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $cliente = new Cliente();
            $cliente->nome = $request->nomeCliente;
            $cliente->nascimento = $request->nascimentoCliente;
            $cliente->cpf = $request->cpfCliente;
            $cliente->telefone = $request->telefoneCliente;
            $cliente->email = $request->emailCliente;

            $cliente->save();

            Session::flash('messagem', 'Parabéns, cliente adicionado com sucesso.');
            Session::flash('class', 'alert-success');
            return Redirect::to('/cliente');
        } catch (\Exception  $errors) {
            Session::flash('messagem', 'Ops ERRO!!, não foi possível adicionar cliente.');
            Session::flash('class', 'alert-danger');
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $cliente = Cliente::get();

        return Datatables::of($cliente)
            ->editColumn('acao', function ($cliente) {
                return '
                    <div class="btn-group btn-group-sm">
                        <a href="/cliente/' . $cliente->id . '/edit"
                            class="btn btn-info"
                            title="Editar" data-toggle="tooltip">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                        <a href="#"
                            class="btn btn-danger btnExcluir"
                            data-id="' . $cliente->id . '"
                            title="Excluir" data-toggle="tooltip">
                            <i class="fas fa-trash"></i>
                        </a>
                    </div>';
            })
            ->escapeColumns([0])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);
        return view('Cliente.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $cliente = Cliente::find($id);
            $cliente->nome = $request->nomeCliente;
            $cliente->nascimento = $request->nascimentoCliente;
            $cliente->cpf = $request->cpfCliente;
            $cliente->telefone = $request->telefoneCliente;
            $cliente->email = $request->emailCliente;

            $cliente->save();

            Session::flash('messagem', 'Parabéns, cliente alterado com sucesso.');
            Session::flash('class', 'alert-success');
            return Redirect::to('/cliente');
        } catch (\Exception  $errors) {
            Session::flash('messagem', 'Ops ERRO!!, não foi possível alterar cliente.');
            Session::flash('class', 'alert-danger');
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Cliente::destroy($id);
            return response()->json(array('status' => "OK"));
        } catch (\Exception  $errors) {
            return response()->json(array('erro' => "$errors"));
        }
    }
}
