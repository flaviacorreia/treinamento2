$(document).ready(function ($)
{
    base_url = window.location.origin;
    var table = $("#clientes").DataTable({

        ajax: base_url + "/cliente/show",
        serverSide: true,
        reponsive: true,
        processing: true,
        searching: true,
        
        "order": [0, "desc"],
        columns: [
            { "width": "10%", data: "id", name: "id" },
            { "width": "25%", data: "nome", name: "nome" },
            { "width": "5%", data: "nascimento", name: "nascimento" },
            { "width": "5%", data: "cpf", name: "cpf" },
            { "width": "35%", data: "telefone", name: "telefone" },
            { "width": "5%", data: "email", name: "email" },
            { "width": "10%", data: "acao", name: "acao" },
        ],
    });

    //adicionando botões na tabela
    new $.fn.dataTable.Buttons(table, {
        buttons: [
            "copy", "csv", "excel", "pdf", "print", "colvis"
        ]
    });

    //setando localização dos buttons na blade
    table.buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');



});

$(document).on("click", ".btnExcluir", function ()
{
    id = $(this).data('id');
    $.ajax({
        type: "delete",
        url: base_url + "/cliente/" + id,
        dataType: 'json',
        crossDomain: true,
        contentType: "application/json",
        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
        success: function ()
        {
            location.reload();
        },
        error: function ()
        {
            alert("Não foi possível excluir!");
        }

    });

});