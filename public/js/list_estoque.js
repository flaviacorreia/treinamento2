$(document).ready(function ($)
{
    base_url = window.location.origin;

    //inicialização da table
    var table = $("#estoques").DataTable({

        ajax: base_url + "/estoque/show",
        serverSide: true,
        reponsive: true,
        processing: true,
        searching: true,

        "order": [0, "desc"],
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        
        columns: [
            { "width": "10%", data: "id", name: "id" },
            { "width": "25%", data: "titulo", name: "titulo" },
            { "width": "5%", data: "valor", name: "valor" },
            { "width": "5%", data: "volume", name: "volume" },
            { "width": "5%", data: "quantidade", name: "quantidade" },
            { "width": "5%", data: "flag", name: "flag" },
            { "width": "10%", data: "acao", name: "acao" },
        ],
    })

    //adicionando botões na tabela
    new $.fn.dataTable.Buttons(table, {
        buttons: [
            "copy", "csv", "excel", "pdf", "print", "colvis"
        ]
    });

    //setando localização dos buttons na blade
    table.buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


});

$(document).on("click", ".btnExcluir", function ()
{
    id = $(this).data('id');
    $.ajax({
        type: "delete",
        url: base_url + "/estoque/" + id,
        dataType: 'json',
        crossDomain: true,
        contentType: "application/json",
        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
        success: function ()
        {
            location.reload();
        },
        error: function ()
        {
            alert("Não foi possível excluir!");
        }

    });

});