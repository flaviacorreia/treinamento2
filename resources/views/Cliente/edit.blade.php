@extends('layouts.app')

@section('htmlheader_title', 'Cliente')
@section('conteudo')


<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Alterar Cliente: {{$cliente->nome}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active">Cliente</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    (<span style="color: red;">*</span>) Campos Obrigatórios
                    <div class="float-right">
                        <a href="{{ URL::to('cliente') }}" class="btn btn-block btn-outline-info "><i class="fa fa-list-alt"></i> Listar</a>
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('messagem'))
                    <div class="alert {{ Session::get('class') }} alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5>Atenção</h5>
                        {{ Session::get('messagem') }}
                    </div>
                    @endif

                    <form method="POST" action="/cliente/{{$cliente->id}}" id="form">
                        @csrf
                        @method('PUT')
                        <div class="form-row">

                            <!-- NOME -->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <strong>Nome<span style="color: red;">*</span></strong>
                                    <input type="text" autocomplete="off" name="nomeCliente" class="form-control @error('nomeCliente') is-invalid @enderror" value="{{ $cliente->nome }}">
                                    @error('nomeCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>


                                <!-- NASCIMENTO -->
                                <div class="form-group col-md-3">
                                    <strong>Nascimento<span style="color: red;">*</span></strong>
                                    <input type="date" autocomplete="off" name="nascimentoCliente" class="form-control @error('nascimentoCliente') is-invalid @enderror" value="{{ $cliente->nascimento }}">
                                    @error('nascimentoCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <!-- CPF -->
                                <div class="form-group col-md-3">
                                    <strong>CPF<span style="color: red;">*</span></strong>
                                    <input type="text" autocomplete="off" name="cpfCliente" class="form-control @error('cpfCliente') is-invalid @enderror" value="{{ $cliente->cpf }}">
                                    @error('cpfCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <!-- TELEFONE -->
                                <div class="form-group col-md-3">
                                    <strong>Telefone<span style="color: red;">*</span></strong>
                                    <input type="telefone" autocomplete="off" name="telefoneCliente" class="form-control @error('telefoneCliente') is-invalid @enderror" value="{{ $cliente->telefone }}">
                                    @error('telefoneCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <!-- EMAIL -->
                                <div class="form-group col-md-6">
                                    <strong>E-mail<span style="color: red;">*</span></strong>
                                    <input type="text" autocomplete="off" name="emailCliente" class="form-control @error('emailCliente') is-invalid @enderror" value="{{ $cliente->email }}">
                                    @error('emailCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <hr>
                        <button type="submit" form="form" class="btn btn-info float-right" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>
                            &nbsp Aguarde...">Salvar</button>

                        <!-- /.card-body -->
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</section>

@endsection
@section('scripts_adicionais')

@endsection