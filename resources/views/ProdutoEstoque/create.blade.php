@extends('layouts.app')

@section('htmlheader_title', 'Produto')

@section('conteudo')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Cadastrar produto</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active"><a href="/produto-estoque">Produtos</a></li>
                    <li class="breadcrumb-item active">Cadastrar</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    (<span style="color: red;">*</span>) Campos Obrigatórios
                    <div class="float-right">
                        <a href="/produto-estoque" class="btn btn-block btn-outline-info "><i class="fa fa-list-alt"></i>
                            Listar produtos</a>
                    </div>
                </div>
                <form action="/produto-estoque" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-3">
                            <label>Título</label> <br>
                            <input type="text" name="titulo_produto" class="form-control"><br><br>
                        </div>
                        <div class="form-group col-2">
                            <label>Valor</label> <br>
                            <input type="text" name="valor_produto" class="form-control"><br><br>
                        </div>
                        <div class="form-group col-2">
                            <label>Volume</label> <br>
                            <input type="text" name="volume_produto" class="form-control"><br><br>
                        </div>
                        <div class="form-group col-3">
                            <label>Descrição</label> <br>
                            <textarea type="text" row="10" name="descricao_produto" class="form-control"></textarea><br><br>
                        </div>
                        <div> <button type="submit" class="btn btn-info float-right" style="margin:32px 0 0 50px;">Cadastrar</button> </div>
                    </div>
                </form>

</section>

@endsection
@section('scripts_adicionais')

@endsection