@extends('layouts.app')

@section('htmlheader_title', 'Produto')

@section('conteudo')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Editar produto</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active"><a href="/produto-estoque">Produtos</a></li>
                    <li class="breadcrumb-item active">Editar</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    (<span style="color: red;">*</span>) Campos Obrigatórios
                    <div class="float-right">
                        <a href="/produto-estoque" class="btn btn-block btn-outline-info "><i class="fa fa-list-alt"></i>
                            Listar produtos</a>
                    </div>
                </div>
                <form action="/produto-estoque/{{$produto_estoque->id}}" method="POST">
                    @csrf
                    @method('PUT')
                    <label>Título</label> <br>
                    <input type="text" name="titulo_produto" value="{{$produto_estoque->titulo}}"><br><br>

                    <label>Valor</label> <br>
                    <input type="text" name="valor_produto" value="{{$produto_estoque->valor}}"><br><br>

                    <label>Volume</label> <br>
                    <input type="text" name="volume_produto" value="{{$produto_estoque->volume}}"><br><br>

                    <label>Descrição</label> <br>
                    <textarea type="text" row="10" name="descricao_produto">{{$produto_estoque->descricao}}</textarea><br><br>

                    <button type="submit">Salvar</button>
                </form>
</section>

@endsection
@section('scripts_adicionais')

@endsection