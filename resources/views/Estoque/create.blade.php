@extends('layouts.app')

@section('htmlheader_title', 'Estoque')

@section('conteudo')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Cadastrar estoque</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active"><a href="/estoque">Estoque</a></li>
                    <li class="breadcrumb-item active">Cadastrar</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    (<span style="color: red;">*</span>) Campos Obrigatórios
                    <div class="float-right">
                        <a href="/estoque" class="btn btn-block btn-outline-info "><i class="fa fa-list-alt"></i>
                            Listar estoque</a>
                    </div>
                </div>
                <form action="/estoque" method="POST">
                    @csrf
                    <div class="form-row">

                        <div class="form-group col-2">
                            <label>Produto</label> <br>
                            <select name='produto'>
                                <option value="">Selecione</option>
                                @foreach($produto_estoque as $produto)
                                <option value="{{$produto->id}}">{{$produto->titulo}}</option>
                                @endforeach
                            </select><br><br>
                        </div>

                        <div class="form-group col-2">
                            <label>Tipo</label> <br>
                            <select name="tipo">
                                <option value="">Selecione</option>
                                <option value='entrada'>Entrada</option>
                                <option value='saida'>Saída</option>
                            </select><br><br>
                        </div>

                        <div class="form-group col-3">
                            <label>Quantidade</label> <br>
                            <input type="number" name="quantidade_produto"><br><br>
                        </div>

                        <div>
                            <button type="submit" class="btn btn-info float-right" style="margin:32px 0 0 50px;">
                                Cadastrar
                            </button>
                        </div>

                    </div>
                </form>

</section>

@endsection
@section('scripts_adicionais')

@endsection