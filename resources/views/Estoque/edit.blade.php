@extends('layouts.app')

@section('htmlheader_title', 'Estoque')

@section('conteudo')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Editar estoque</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active"><a href="/estoque">Estoque</a></li>
                    <li class="breadcrumb-item active">Editar</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    (<span style="color: red;">*</span>) Campos Obrigatórios
                    <div class="float-right">
                        <a href="/estoque" class="btn btn-block btn-outline-info "><i class="fa fa-list-alt"></i>
                            Listar estoque</a>
                    </div>
                </div>
                <form action="/estoque/{{$estoque->id}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-row">

                        <div class="form-group col-2">
                            <label>Produto</label> <br>
                            <select name='produto'>
                                <option value="">Selecione</option>
                                @foreach($produto_estoque as $produto)
                                <option value={{$produto->id}} @if($produto->id==$estoque->fk_produto_estoque)
                                    selected
                                    @endif>
                                    {{$produto->titulo}}
                                </option>
                                @endforeach
                            </select><br><br>
                        </div>

                        <div class="form-group col-2">
                            <label>Tipo</label> <br>
                            <select name="tipo">
                                <option value="" @if($estoque->flag=="") selected @endif>Selecione</option>
                                <option value="entrada" @if($estoque->flag=="entrada") selected @endif>Entrada
                                </option>
                                <option value="saida" @if($estoque->flag=="saida") selected @endif>Saída</option>
                            </select><br><br>
                        </div>

                        <div class="form-group col-3">

                            <label>Quantidade</label><br>
                            <input type="number" name="quantidade_produto" value="{{$estoque->quantidade}}"><br><br>
                        </div>

                        <div>
                            <button type="submit" class="btn btn-info float-right" style="margin:32px 0 0 50px;">
                                Salvar
                            </button>
                        </div>
                </form>
</section>

@endsection
@section('scripts_adicionais')

@endsection