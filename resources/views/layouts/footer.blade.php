    <!-- Main Footer -->
    <footer class="main-footer d-flex justify-content-between">
        <strong>Copyright © 2022</strong>

        <strong>All rights reserved.</strong>
        <a href="https://linkedin.com/in/fllaviacorreia">Flávia Correia</a>

        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 1.0
        </div>
    </footer>