<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-4 " style="background-color: #000;">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
        <img src="{{asset('plugins/AdminLTE/dist/img/Logo.png')}}" alt="AdminLTE Logo"
            class=" img-rectangle elevation-3 mx-auto d-block" style="width: 100%;" />
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('plugins/AdminLTE/dist/img/user1.png')}}" class="img-circle elevation-2"
                    alt="User Image" />
            </div>
            <div class="info">
                <a href="" class="d-block">Flávia Correia</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

                <li class="nav-item menu-open">
                    <a href="/home" class="nav-link active">
                        <p>
                            Página inicial
                        </p>
                    </a>
                </li>

                <li class="nav-item menu-open">
                    <a href="/cliente" class="nav-link active">
                        <p>
                            Clientes
                        </p>
                    </a>
                </li>

                <li class="nav-item menu-open">
                    <a href="/produto-estoque" class="nav-link active">
                        <p>
                            Produtos
                        </p>
                    </a>
                </li>

                <li class="nav-item menu-open">
                    <a href="/estoque" class="nav-link active">
                        <p>
                            Estoque
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>