<!-- jQuery -->
<script src="{{ asset('plugins/AdminLTE/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap -->
<script src="{{ asset('plugins/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- AdminLTE -->
<script src="{{ asset('plugins/AdminLTE/dist/js/adminlte.js')}}"></script>